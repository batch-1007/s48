const csv = require('csvtojson')
// "fs" is Node's file system, required to write files in the file system
const fs = require('fs')

module.exports.convert = async (params) => {

    //file upload  -> base64String -> passed into the backedn api -> base64 string rebuilt as a csv file

    // Use the current date and time as the file name with the filetype hardcoded as csv
    // This is also done to ensure that all files uploaded would have a unique name to avoid file replacement
    const fileName = `${Date.now()}.csv`;

    // Get the base64 encoded data from the request body in the route
    const base64String = params.csvData;

    // Prepare the base64 encoded string to be rebuilt back to csv format
    const base64Csv = base64String.split(';base64,').pop();

    // Designated file path where file will be written, directory HAS TO EXIST, will NOT be created if non-existent at time of writing
    const csvFilePath = `uploads/${fileName}`;

    // Save the CSV file in the designated file path(uploads folder)
    await fs.writeFile(csvFilePath, base64Csv, {encoding: 'base64'}, () => {});

    // Convert csv to json using the fromFile() method of the object created from csvtojson's csv() constructor
    const jsonArray = await csv().fromFile(csvFilePath);
    //this is the display of the newly created array of json adter the csv 
    console.log(jsonArray);
    return {jsonArray: jsonArray};
}

//flow:
//client : file upload > base64String > Passed into the backend api > base64 string rebuilt as a csv file > converted into an array of JSON > return as a response to the Client (frontend)