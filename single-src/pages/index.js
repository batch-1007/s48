import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import { Form, Button } from 'react-bootstrap';
import styles from '../styles/Home.module.css'

import PieChart from '../components/PieChart';
import BarChart from '../components/BarChart';
import { Bar } from 'react-chartjs-2';


export default function Home() {
  const [csvData, setCsvData] = useState([]);
  const [brands, setBrands] = useState([]);
  const [salesByBrand, setSalesByBrand] = useState([]);
  //create a reference to the file input element
  const fileRef = React.createRef()


//base 64 is one of the oldest encoding algorithm
//toBase64 function here will alow us to encode our uploaded file into a base64String with which we can pass to our backend api. 
  const toBase64 = (file) => new Promise((resolve, reject) =>{
    //File reader will alow us to read and translate our file into base64String
      const reader = new FileReader()
      //we pass our file into the readAsDataURL() method of our FileReader to actually convert our file into base64 string
      reader.readAsDataURL(file)
      //onload runs when theb translation or conversion from file to base64 is complted.
      //the base64sting is now in the result property of our reader.
      reader.onload = () => {

        console.log(reader.result)
        return resolve(reader.result)
      }
      
      //onerror will run if the conversion of the file to base64 is not completed and an error has been thrown. 
      reader.onerror = error => reject(error)
  })

  //function to submit CSV to API for convension to JSON data 
  function uploadCSV(e){
    e.preventDefault()
    //fileRef.current.files[0]- a reference to input element where the file was uploaded with.
    toBase64(fileRef.current.files[0])
    .then(encodeFile =>{
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/cars`,{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({csvData: encodeFile})
      })
      .then(res => res.json())
      .then(data =>{

        //csv was read and converted to an array of JSON in the backend, this is the reason the backend's response  is an array of JSOn instead of CSV file.
        console.log(data)

        if(data.jsonArray.length > 0 ){
          //set raw response of our API as teh csvdata state
          setCsvData(data.jsonArray)
        }
      })
    })
  }

  // Use effect will run on initila render and when our CSV data state is updated
  useEffect(() => {
    //This useeffect will try to get all the distinc brands/makes of the cars in our csvData. 
    //makes will be our temporary array where we will push the distinct brands available. 
    let makes = []
    //iterate over all  the items in the csv Data array (which containes data coming from csv)
    csvData.forEach(element =>{
      if(!makes.find(make => make === element.make)){
        //push car Make info to the makes array
        makes.push(element.make)
      }
    })
    // console.log(makes)
    setBrands(makes)
  }, [csvData])


  //We're going get the accumulated sales for each brand
  //create a use effect that will run on initial render and when the brands state is updated

  useEffect(()=>{
    //we will set and update our salesBy Brand state by using map. This map will return an object which will contain the brand name and its appropriate sales. 
    // map returns new array so mababago si setSalesByBrand 
    setSalesByBrand(brands.map(brand => {
        //set/create a variable which will store the acumulated sales of a brand: 
        let sales  = 0;
        //iterate over all the items in the csvData array
        //putting foreach inside a map will result in a nested loop;
        //brands[0] -. forEach() will be finsihed first -> brands[1]
        csvData.forEach(element =>{
          //for each element in the array we will accumulate the sales per brand
          // if the current item being iterated by forEach() has the same brand or make with the current item/brand being iterated by the map, we will accumulate the element's sales into the sales variable however, if the make property or the brand of the current item iterated by for each does not match the current brand being iterated by map, we will simply do nothing
          if(element.make === brand){

            sales += parseInt(element.sales)

          }

        }) 
        // console.log(`${brand}: ${sales}`)

        return {
          brand:brand,
          sales:sales
        }
      
    }))
  }, [brands])
      

 


  //check satte values after file upload
  // console.log(csvData)
  // console.log(brands)
  // console.log(salesByBrand)

  return (
      <React.Fragment>
        <Head>
          <title>CSV DAta Visualization</title>
        </Head>

        <Form onSubmit ={ (e) => uploadCSV(e)}>
            <Form.Group controlId="csvUploadForm">
                <Form.Label>Upload CSV:</Form.Label>
                <Form.Control
                    type="file"
                    ref={ fileRef }
                    accept="csv"
                    required
                />
            </Form.Group>
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
        </Form>
        <PieChart salesData={salesByBrand}/>
        <BarChart rawData={csvData}/>
      </React.Fragment>
  )
}
