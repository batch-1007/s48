import { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function BarChart( {rawData} ){

    //States to store the array of months/labels and monthSales/amounts
    const [ months, setMonths ] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);

    const [monthlySales, setMonthlySales] = useState([]);
    console.log(rawData)

    useEffect(() => {

        setMonthlySales(months.map(month => {
            
            let sales= 0;
            rawData.forEach(element => {

                //moment is a function na tatanggap lag ng date
                //element.sale_date=  7/15/2020
                // month = "Jnuary"

                //moment syntax = moment(date).format(format)
                // console.log(moment(element.sale_date).format('MMMM'))

                if(moment(element.sale_date).format('MMMM') === month) {
                    //naka string si element.sales
                    sales = sales + parseInt(element.sales);
                }

            })
            return sales;
        }))
        
    }, [rawData])

    console.log(monthlySales)
    
    const data = {
        labels: months,
        datasets: [{
            label: 'Monthly Sales for Year 2020',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,0.2)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,0.4)',
            data: monthlySales
        }]
    }
    
    return(
        <Bar data={data} />
    )
}