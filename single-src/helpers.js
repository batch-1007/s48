const colorRandomizer =() =>{
    return(
        //16777215 is the color white max value
        //
        Math.floor(Math.random()*16777215).toString(16)
    )
}

const helloWorld =() =>{
    alert('This is test hello world')
}

export { colorRandomizer, helloWorld };